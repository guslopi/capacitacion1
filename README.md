__________________________________________________________________________________
__________________________________________________________________________________

# [![Invie](README-src/invie.png)](https://gitlab.com/edgarvaldez99/capacitacion1)
__________________________________________________________________________________

# **Capacitación 1**
__________________________________________________________________________________

### Venta de guitarras

Este es un Proyecto de prueba el cual se utilizará solo para la capacitación sobre HTML y CSS.

### Requisitos para empezar con la capacitación:

* **Tener instalado [git](https://platzi.com/blog/introduccion-git/ "Introducción a git"):**
	* _En [linux o Mac](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git#Installing-on-Linux "Installing on Linux or Mac"):_
		
		> `sudo apt-get install git`

	* _En [windows](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git#Installing-on-Windows "Installing on Windows"):_ Leer la documentación para más información. Una vez instalado ___usar la terminal que viene con la aplicación___, no usar la terminar de windows puesto que no se recomienda su uso con git.

* **Editores de textos recomendados:** *[SublimeText](https://www.sublimetext.com/)*, *[Atom](https://atom.io/)*

### Temática del curso:

El curso esta compuesto por 26 pasos, en cada paso hay requisitos o funcionalidades a terminar, *[los cuales se detallan más abajo en este documento](#descripci%C3%B3n-de-los-pasos-a-realizarse)*.

Para ver la evolución del proyecto, *[el cual se necesita descargar como se indicará más abajo en este documento](#primeros-pasos)*, hay que ejecutar una serie de comandos en la consola dependiendo de cada acción, por ejemplo, para ver el código y los cambios de cierto número de paso o listar todos los pasos, etc. *A continuación se detallan los mismos*:
* Para ver la lista de pasos del proyecto *(Se lista números que corresponden a los pasos, _[un total de 26]_)*:

	> `git tag`

* Para ver el código y los cambios realizados de un cierto número de paso. *__Ejemplo__ ver el código del paso 01*:

	> `git checkout 01`

* Para ver una descripción detallada de cada paso:

	> `git show 01`

* Para ver la diferencia entre un paso y otro:

	> `git diff 01 02`

* Para deshacer los cambios:

	> `git reset --hard`

	> Cuando tienes cambios hechos y quieres pasar al siguiente paso con checkout, git no te dejará hacerlo puesto que él requiere que cometas esos cambios, para no hacerlo usa reset --hard.

Con cada una de las acciones ya mencionadas se detalla el flujo de trabajo el cual consiste en verificar lo realizado en cada paso con el `checkout`. Empezaremos con el paso 00 escribiendo en la terminal `git checkout 00`, si realizamos algún cambio escribimos `git reset --hard` para luego pasar al cambio siguiente con `git checkout 01`, para ver la diferencia entre el cambio 00 y 01 escribimos `git diff 00 01`, una vez terminado con el cambio 01 pasamos al siguiente y realizamos lo mismo hasta llegar al último paso.

### Primeros pasos:

* Ingresar a la terminal
* Pegar el siguiente comando (Con este se descarga el proyecto):

	> `git clone `[https://gitlab.com/edgarvaldez99/capacitacion1.git](https://gitlab.com/edgarvaldez99/capacitacion1)

* Luego pasar al paso 00 con el siguiente comando

	> `git checkout 00`

* Una vez que estemos en el paso [00](https://gitlab.com/edgarvaldez99/capacitacion1/tags/00), ver la imagen demo, en el cual se muestra la forma en que debería verse el sitio web al terminar esta capacitación. 

A partir de aquí se describen cada uno de los pasos para lograr terminar el sitio como se muestra en la imagen mencionada.

## Descripción de los pasos a realizarse

En cada ítem se describe el trabajo a realizarse, seguido del objetivo que se pretende lograr con el mismo y en algunos casos tendrán un resumen o conclusión. _Acontinuación se detalla los mismos_:

* __[01](https://gitlab.com/edgarvaldez99/capacitacion1/tags/01)__: Para empezar, crear la estructura de carpetas del proyecto y el archivo HTML principal, es decir, el archivo index. La estructura puede ser a elección pero como sugerencia se ofrece la siguiente:

		capacitacion1/
			invie/
				css/
				images/
				index.html
			demo.png
			LICENSE
			README.md
			README-src/

* __[02](https://gitlab.com/edgarvaldez99/capacitacion1/tags/02)__: Empezando con la maquetación HTML del proyecto. *Entender la sitaxis, los usos y tipos de etiquetas, los atributos y la estructura básica de un documento HTML.*

	[Introducción a HTML, etiquetas y atributos](https://developer.mozilla.org/es/docs/Web/Guide/HTML/Introduction_alhtml)

	***Conclusión:***
	* __Sintaxis__: 
		* _Elementos de bloques_:

            ```html
            <etiqueta atributo1="valor1" ... atributoN="valorN" >
                Contenido
            </etiqueta>
            ```

		* _Elementos en línea_:

		    ```html
            <etiqueta atributo1="valor1" ... atributoN="valorN" />
            ```

	* __Para definir Tipo de Documento__: 
		* _HTML5_: `<!DOCTYPE html>`
		* _HTML4_: `<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">`
	* __La estructura básica__:
		* _Tipo de documento_: `<!DOCTYPE html>`
		* _Etiqueta Raíz_ `<html></html>`:
			Dentro de él va:
			* La cabeza del documento `<head></head>`:

				> Aquí va la información del documento y los elementos que necesita para cargarse.
			* El cuerpo del documento `<body></body>`:

				> Aquí se carga todo lo que va a ver el usuario.

		A esta estructura jerárquica se lo denomina DOM.

	---

* __[03](https://gitlab.com/edgarvaldez99/capacitacion1/tags/03)__: Continuando con maquetación y el uso de las etiquetas. Crear la estructura básica del cuerpo del documento HTML empezando por las [etiquetas semánticas](http://www.w3schools.com/html/html5_semantic_elements.asp). *Entender la diferencia entre las etiquetas de presentación o interacción y las que agregan valor semántico.*

	[Etiquetas semánticas del HTML5](http://www.desarrolloweb.com/articulos/etiquetas-semanticas-html5.html)

	[Lista de Elementos HTML5](https://developer.mozilla.org/es/docs/HTML/HTML5/HTML5_lista_elementos)

	En este paso el trabajo consiste en identificar las siguientes etiquetas dentro de la imagen demo:

	![Ejemplo 1 de etiquetas semánticas](README-src/semantic1.png) ![Ejemplo 2 de etiquetas semánticas](README-src/semantic2.jpg)

	***Conclusión:***
	* _Antes de empezar con las etiquetas lo recomendado es escribir texto que represente lo que queremos hacer, por ejemplo 'Aquí estará el logo', 'En esta parte estará el menu', etc, para luego reemplazarlo por la etiqueta que corresponde_.
	* __Existen 2 clases de etiquetas__:
		* _Las que agregan presentación o interacción_: Formatean la salida de su contenido o le agregan alguna acción.
		* _Las que agregan valor semántico_: Indican qué es el contenido que contienen.
	* __Como una buena práctica, en HTML5, es recomendado empezar a maquetar con las etiquetas semánticas__.

	---

* __[04](https://gitlab.com/edgarvaldez99/capacitacion1/tags/04)__: Terminando con la maquetación del proyecto, ampliando el conocimento y uso de las etiquetas. Esta vez toca trabajar con las etiquetas de presentación. *Entender el uso de las etiquetas, cuando usar o no ciertas etiquetas.*

	***Conclusión:***
	* _Después de la maquetación semántica vienen las etiquetas de presentación_.
	* _Esta vez decidir cada etiqueta depende de lo que queremos mostrar, por ejemplo: 'Aquí estará una lista' entonces reemplazar por un `<ul>`, 'Aquí va una imagen' entonces reemplazar por un `<img>`, etc_.

	---

* __[05](https://gitlab.com/edgarvaldez99/capacitacion1/tags/05)__: Agregando las imágenes a utilizarse en el proyecto y las referencias a las mismas desde el documento HTML. *Entender el como referenciar y dimencionar a las imágenes en nuestro documento HTML.*

	***Conslusión:***
	* Las imágenes son referenciadas con el atributo `src` de la etiqueta `<img>`. La ruta debe ser relativa, estilo unix.
	* Si la dimensión no es especificada en el documento entonces se usa la de la imagen. Con especificar ancho es suficiente puesto que la altura se calcula en proporción a esta.

	---

* __[06](https://gitlab.com/edgarvaldez99/capacitacion1/tags/06)__: Agregando los enlaces para dar una navegación básica al sitio. *Entendiendo el uso de la etiqueta `<a>`_(ancla)_, como agregar referencias internas y externas.*

	***Conclusión:***
	* Con la etiqueta `<a>` podemos agregar navegación a un documento HTML a través de su atributo `href`.
	* __Existen 2 formas__:
		* _Navegación Externa_: Como en las imágenes, utiliza una ruta relativa tipo unix, solo que esta vez hace referencia a otro documento HTML.
		* _Navegación Interna_: No se necesita una ruta, solo el caracter '#' seguido del id de la etiqueta al cual queremos referenciar.

	---

* __[07](https://gitlab.com/edgarvaldez99/capacitacion1/tags/07)__: Agregando estilos al proyecto. En este paso se tiene que agregar estilos al proyecto de las tres formas más utilizadas: [__En línea__ o __De etiqueta__], [__Interno__ o __Del documento__] y [__Externo__]. [En total existen 4 formas de agregar estilos a un documento HTML.](http://www.maestrosdelweb.com/cssestilos/) *Entender las tres formas más utilizadas de agregar estilos a un documento HTML.*

	***Conslusión:***

	* __Formas de agregar estilos a un documento HTML__:
		* __En línea__: Los estilos se agregan como valor del atributo style el cual poseen todas las etiquetas. Esta forma no es recomendada puesto que no cumple con la filosofía de CSS que es la de separar la estructura de su presentación.
		* __Interno o Del documento__: Los estilos se agregan como contenido de la etiqueta `<style>` en cualquier parte del documento HTML, pero es recomendado ponerlo dentro del `<head>` o al final del `<body>`
		* __Archivo externo__: Utilizando la etiqueta `<link>` para hacer referencia al archivo css poniendo la ruta como valor de su atributo `href`.
		* __Importando el archivo__: Con la siguiente sintaxis `@import "import.css"` se puede importar un archivo css desde otro archivo o desde el mismo documento HTML, poniendo el import como contenido de una etiqueta `<style>`

	---

* __[08](https://gitlab.com/edgarvaldez99/capacitacion1/tags/08)__: Agregando más estilos al proyecto. En el archivo css externo agregar más estilos con los distintos tipos de selectores. *Entendiendo el uso y tipos de selectores.*

	[30 Selectores que debes conocer](http://code.tutsplus.com/es/tutorials/the-30-css-selectors-you-must-memorize--net-16048)

	***Conclusión:***
	* La sintaxis en CSS es la siguiente:

		```css
		selector {
			propiedad: valor;
		}
		```

	* Puede que uno no llegue a conocer todas las propiedades que existen en CSS para dar estilos, pero si es muy importante conocer a todos los [selectores](http://code.tutsplus.com/es/tutorials/the-30-css-selectors-you-must-memorize--net-16048) que sea posible.

	---

* __[09](https://gitlab.com/edgarvaldez99/capacitacion1/tags/09)__: Continuando con los estilos a las etiquetas HTML. En este paso el trabajo consiste en jugar con los estilos para entender mejor el orden de prioridades de CSS. _Entendiendo el orden de prioridades de las declaraciones en CSS, es decir, como estos son aplicados (En cascada)_.

	***Conclusión:***
	* __Prioridad de las declaraciones en CSS (Orden de Cascada)__:
		
		Es habitual que a una etiqueta se le aplique un mismo estilo varias veces, a esto se lo conoce como [colisión de estilos](http://librosweb.es/libro/css/capitulo_2/colisiones_de_estilos.html), para resolver esto CSS prioriza los estilos guiandose por las siguientes normas:
		* __Según el autor u origen__: _Según el autor los estilos se aplican con el siguiente orden (De menos importante a más importante)_:
			* Estilos del Navegador.
			* Estilos del Usuario.
			* Estilos del Diseñador o Frontend.
			* Estilos del Diseñador o Frontend con la palabra reservada `!important`.
			* Estilos del Usuario con la palabra reservada `!important`.

			[Para más información haga click aquí](http://librosweb.es/libro/css/capitulo_13/prioridad_de_las_declaraciones_css.html)

		* __Según el selector__: _Según la especificidad del selector el orden a aplicarse es_:
			![Prioridad según selector](README-src/especificidadCSS.png "Prioridad según selector")
		* __Según la ubicación del estilo__: _Según la ubicación donde aparece el estilo el orden de la cascada es la siguiente(De menos importante a más importante)_:
			* Archivo externo.
			* Del documento HTML (Interno).
			* De la etiqueta (En línea).
		* __Según la posición del estilo__: _Si después de aplicar las normas anteriores existen dos o más estilos con la misma prioridad, entonces se aplica la que se indicó en último lugar(efecto cascada)._

	---

* __[10](https://gitlab.com/edgarvaldez99/capacitacion1/tags/10)__: Agregando clases a nuestras etiquetas para poder darles estilos desde el archivo css. *Entendiendo el porqué del uso del atributo class en HTML para aplicar estilos desde css.*

	***Conclusión:***

	El porqué del uso de clases para aplicar estilos:

	* El valor del atributo `class` puede repetirse entre etiquetas.
	* Una etiqueta puede poseer una o varias clases.
	* Para poder evitar el problema de la especificidad de selectores.

	---

* __[11](https://gitlab.com/edgarvaldez99/capacitacion1/tags/11)__: Empezando con los estilos más básicos como lo son los backgrounds, colores y los estilos para las fuentes. Se empieza con estos estilos puesto que son considerados los más básicos en css, pero eso solo es una opción. *Entendiendo las siguientes propiedades: `color`, las relacionadas con `font` y los distintos usos de `background`. Entender que existen propiedades que pueden heredarse y otras que no, dar un ejemplo de cuales se heredan y cuales no.*

	***Conclusión:***
	* Existen etiquetas que son heredables y otras que no. Todas las estiquetas que tienen como posible valor a `inherit` son heredables, [consultar la documentación para más información](http://www.w3schools.com/css/).
	* Empezar a dar estilos a las fuentes, colores y fondos es una buena recomendación ya que los mismos son los más faciles de aplicar.

	---

* __[12](https://gitlab.com/edgarvaldez99/capacitacion1/tags/12)__: Agregando fuentes al proyecto. *Entendiendo como agregar fuentes que podrían no ser reconocidos por el sistema operativo, entendiendo la manera optima de agregarlos.*

	***Conclusión:***

	En un proyecto web se pueden usar todas las fuentes que queramos, pero como no todas son reconocidas por el sistema operativo del usuario surge la necesidad de agregarlos.  
	Existen varias páginas que nos ayudan a lograr este objetivo y las más conocidas son [Google Fonts](https://fonts.google.com/) y [Font Squirrel](https://www.fontsquirrel.com/).

	---

* __[13](https://gitlab.com/edgarvaldez99/capacitacion1/tags/13)__: Agregando los estilos para editar el __Modelo de Caja__, para ello necesitamos aplicar las siguientes propiedades: `border`, `margin`, `padding`, `height`, `width`. *Entendiendo el Modelo de Caja y las propiedades que permiten editarlo.*

	[Un pequeño resumen para entender el modelo de cajas](http://w3.unpocodetodo.info/css3/box-sizing.php)

	[Entendiendo el modelo de caja para los elementos en línea](https://www.mozilla-hispano.org/modelo-de-cajas-css-para-elementos-en-linea/)

	***Conclusión:***

	El estandar especifica que:
	* `margin` (margen) + `border` (borde) + `padding` (relleno) + `width` (anchura) = anchura real de la caja.
	* `margin` (margen) + `border` (borde) + `padding` (relleno) + `height` (altura) = altura real de la caja.
	* Se puede agregar `margin`, `padding` o `border` solo a uno de los lados de la caja especificandolo, por ejemplo: `margin-left: 30px`.

	__Obs__: Esta especificación puede ser modificada con la propiedad `box-sizing`.

	---

* __[14](https://gitlab.com/edgarvaldez99/capacitacion1/tags/14)__: Continuando con el __Modelo de Caja__. Cambiar la disposición de las etiquetas que necesitan compartir su espacio. *Entendiendo sobre disposiciones en css, la propiedad `display` con los valores: `block`, `inline` e `inline-block`. Entendiendo cuales son las etiquetas que se asignan por defecto con la propiedad `display: block` y a cuales las que se asignan con `display: inline`.*

	[Introducción a la propiedad `display`](http://es.learnlayout.com/display.html)

	[Entendiendo a la propiedad `display`](http://adritah.es/entendiendo-display-float-y-positions-en-css/)

	[Ver ejemplos de su uso](https://jsfiddle.net/edgarvaldez99/58xj085m/)

	***Conclusión:***

	Cada etiqueta HTML ocupa un espacio en la página llamada caja. Por defecto cada caja ocupa el alto de lo que contiene y el 100% de ancho, para romper esta regla existe la propiedad `display` cuyos valores más utilizados son los siguientes:
	* __block__: Características
		* Hace que la caja se adapte a lo que contiene verticalmente pero sigue ocupando el 100% del ancho. 
		* Permite `width`, `height`, `margin` y `padding`. 
		* Todas las etiquetas que pueden contener a otras tienen por defecto este `display`.
	* __inline__: Características
		* Hace que la caja se adapte a lo que contiene vertical y horizantalmente. 
		* Permite `margin` y `padding` left y right, es decir, solo se puede agregar distancia entre cajas inline horizontalmente _(si necesitas ajustar la distancia vertical entre cajas inline puedes usar `line-height`)_.
		* No permite `top`, `bottom`, `height` ni `width`.
		* Permite a otros elementos estar a su derecha o a su izquierda.
		* Las etiquetas que se utilizan para contener o formatear textos poseen este valor por defecto, por ejemplo los `<input>`, `<textarea>`, `<select>`, `<span>`, `<a>`, `<em>`, `<strong>`, etc.
	* __inline-block__: mezcla las caracteristicas del `inline` y del `block`.
	* __none__: la caja no se muestra y desaparece la caja del espacio que ocupaba. _(La diferencia con `visible: hidden` es que éste solo oculta a la caja, no hace desaparecer su espacio)_.

	Existen otros valores que son útiles y muy interesantes los cuales merencen la pena [conocerlos](http://www.w3schools.com/cssref/pr_class_display.asp "Conocer los otros valores de la propiedad display").

	---

* __[15](https://gitlab.com/edgarvaldez99/capacitacion1/tags/15)__: Aplicando lo aprendido de los estilos del __Modelo de Caja__ al sitio para que se vea como en la imagen demo.

	---

* __[16](https://gitlab.com/edgarvaldez99/capacitacion1/tags/16)__: Separando los estilos comunes de las clases para ponerlos en una sola con el objetivo de modularizar los estilos, haciendo así que esas etiquetas posean 2 clases. *Aprovechar la característica del uso de una sola clase en varias etiquetas.*

	---

* __[17](https://gitlab.com/edgarvaldez99/capacitacion1/tags/17)__: Aplicando la propiedad `float` a las imagenes, y los estilos necesarios para que queden como en la imagen demo *Entendiendo el uso de la propiedad `float`, como este modifica el modelo de caja y entendiendo a la propiedades `overflow` y `clear`*

	[Entendiendo a la propiedad `float`](http://adritah.es/entendiendo-display-float-y-positions-en-css/)

	[Ver ejemplo de su uso](https://jsfiddle.net/edgarvaldez99/boxmq29d/)

	***Conclusión:***

	Con la propiedad `display` no tenemos suficiente porque a veces nos interesa que cierto elemento se situe a la izquierda o a la derecha de su contenedor y que el resto se mantega a su alrededor sin solaparse, para esto existe la propiedad `float` con las siguientes características:
	* Las cajas que posean esta propiedad salen del flujo normal del documento para quedar flotando sobre su padre sin solaparse con sus hermanos _(por este motivo es posible que algunos o todos los elementos flotantes se salgan de su elemento contenedor, para resolver este problemas existe [la propiedad `overflow`](http://librosweb.es/libro/css_avanzado/capitulo_1/limpiar_floats.html))_.
	* Se utiliza para colocar cajas a la derecha o a la izquierda del contenedor.
	* Admite `height` y `width`.
	* Se adapta al contenido.
	* El resto de elementos que están situados después de él lo rodean. _(Cuando queremos romper esa inercia se utiliza [la propiedad `clear`](http://librosweb.es/referencia/css/clear.html))_.

	---

* __[18](https://gitlab.com/edgarvaldez99/capacitacion1/tags/18)__: Aplicando el estilo de posición al menu y a las imagenes para poder moverlos a la misma posición que se observa en la imagen demo. *Entendiendo el uso de `position`, las tres más importantes: `relative`, `absolute` y `fixed`.*

	[Entendiendo a la propiedad `position`](http://adritah.es/entendiendo-display-float-y-positions-en-css/)

	[Ver ejemplos de su uso](https://jsfiddle.net/edgarvaldez99/4hc5LvLy/)

	***Conclusión:***

	Valores de la propiedad `position`:
	+ _`position: static`_ ⇒ es la posición por defecto en el page flow, no se puede usar top, bottom, left ni right.
	+ _`position: relative`_ ⇒ el elemento se posiciona respecto a sí mismo.
	+ _`position: absolute`_ ⇒ el elemento se posiciona respecto a un padre a su vez posicionado, su caja sale de su posición original en el page flow y pasa a flotar.
	+ _`position: fixed`_ ⇒ el elemento se posiciona respecto al viewport.

	---

* __[19](https://gitlab.com/edgarvaldez99/capacitacion1/tags/19)__: Agregando bordes redondeados a los elementos que los necesiten, los cuales identificaremos según se vean en la imagen demo. *Entendiendo el uso de los bordes redondeados.*

	[Entendiendo el uso de `border-radius`](http://expertoencss3.com/blog/bordes-redondeados-css3-css3-radius-todos-los-navegadores)

	[Bordes redondeados (Avanzado)](http://paul.pe/blog/2011/03/esquinas-redondeadas-en-todos-los-navegadores-con-css/)

	[Generar bordes redondeados](http://www.cssmatic.com/es/border-radius)

	***Conclusión:***
	Caracteristicas de `border-radius`:
	* En su forma abreviada acepta 4 valores correspondientes al radio de la curvatura de cada una de las 4 esquinas en el siguiente orden: 

		__arriba-izq,  arriba-der, abajo-der, abajo-izq__

	* En su forma no abreviada se puede especificar el valor de cada una de las esquinas, por ejemplo: `border-top-left-radius: 10px;`
	* Se puede crear bordes elípticos especificando 2 radios por cada esquina, por ejemplo: `border-top-left-radius: 10px 5px;`

	---

* __[20](https://gitlab.com/edgarvaldez99/capacitacion1/tags/20)__: Empezando con el footer, Agregar la maquetación para poner los datos del contacto. Continuando con las anclas, aprender las nuevas funcionalidades de las anclas. *Aplicar lo aprendido. Entendiendo el uso de las nuevas funcionalidades de las anclas.*

	[Aprendiendo anclas en HTML](http://www.w3schools.com/tags/tag_a.asp)

	---

* __[21](https://gitlab.com/edgarvaldez99/capacitacion1/tags/21)__:  Terminando con la maquetación del footer, Agregar un formulario para que los clientes puedan ponerse en contacto con la empresa. *Entendiendo el uso de los input y sus distintos tipos, label y textarea, entendiendo las funcionalidades de cada uno de los atributos de estas etiquetas.*

	[Todo sobre formularios en HTML](http://www.html5.showbizreal.com/Columnas/module/formMod.php)

	[Ver ejemplo](https://jsfiddle.net/edgarvaldez99/uq8gh3c6/)

	***Conclusión:***
	* Un formulario es una herramienta que se encarga de gestionar los datos introducidos por el usuario. Dispone de una multitud de controles, cada uno con una utilidad específica (campos de texto, casillas de verificación, etc...).
	* Para que el navegador imprima un formulario en la página web necesita un conjunto básico de etiquetas: `<form>` _(Delimita el area del formulario)_, `<input>` en sus distintos tipos, `<textarea>`, `<select>` _(sirven para obtener datos)_. `<fieldset>`, `<legend>`, `<label>`, `<datalist>`, `<optgroup>` _(estos últimos son opcionales, pero importantes porque con ellas se puede estructurar mejor y añadir funciones interesantes al formulario)_.

	---

* __[22](https://gitlab.com/edgarvaldez99/capacitacion1/tags/22)__: Agregando los estilos al footer, Para ello usar un valor de la propiedad `display` que hasta ahora no se dió a conocer, es decir, nos introduciremos en el uso de `flex`. *Entendiendo el uso de `display: flex`*.

	[Guía completa de `display: flex` _(Ingles)_](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)

	[En qué consiste FlexBox](http://www.emenia.es/flexbox-la-caja-flexible-css3/)

	[Ver ejemplos de su uso](https://jsfiddle.net/edgarvaldez99/gxkxf0qx/)

	***Conclusión:***
	* La declaración `display: flex;` define un _"contenedor flexible"_ y convierte de forma automática a sus _"hijos"_ directos en _"elementos flexibles"_.
	* Puedes poner contenedores flexibles uno dentro de otro, ya que la pripiedad `display` no se hereda.
	* Posee dos ejes:
		* __El eje principal *(main axis)*__: que es la dirección en la cual se posicionan los elementos flexibles.
		* __El eje transversal__: perpendicular al eje principal.
		* Ambos ejes tienen una serie de propiedades que controlan cómo se posiciona cada elemento flexible en relación a los demás.

		A continuación una imagen que hace referencia a los ejes:   
		![Ejes de flexbox](README-src/flexbox.jpg)

	---

* __[23](https://gitlab.com/edgarvaldez99/capacitacion1/tags/23)__:  Continuando con la navegación para el sitio. Crear un nuevo documento HTML, el cual contendrá la lista de los precios de las guitarras. *Entendiendo mejor el uso del ancla, probar el enlace externo.*

	---

* __[24](https://gitlab.com/edgarvaldez99/capacitacion1/tags/24)__: Agregando la tabla con la lista de precios al proyecto, aplicar lo aprendido para agregarle los estilos a la tabla. *Entendiendo el uso de las tablas en HTML, sus partes y reforzar lo aprendido aplicandoles estilos.*

	[Todo sobre tablas en HTML](http://www.html5.showbizreal.com/Columnas/module/tableMod.php)

	---

* __[25](https://gitlab.com/edgarvaldez99/capacitacion1/tags/25)__: Agregando el ícono para los menus a través del uso de pseudo-elementos. *Entendiendo el uso de íconos y pseudo-elementos, cuando y porque usarlos y entender que generalmente las etiquetas de línea(`display: inline`), por ejemplo, los input de formularios y otras etiquetas no poseen pseudo-elementos.*

	[Buscar el ícono en esta página](https://dev.w3.org/html5/html-author/charref)

	[Empezando con los pseudo-elementos](https://developer.mozilla.org/es/docs/Web/CSS/Pseudoelementos)

	[Entendiendo el uso de los pseudo-elementos](http://www.esandra.com/aprende-a-usar-los-pseudo-elementos-before-y-after-en-CSS)

	---

* __[26](https://gitlab.com/edgarvaldez99/capacitacion1/tags/26)__: Puliendo el sitio con todo lo aprendido, repasando y poniendolo en práctica. Observando la imagen demo y comparando con lo que tenemos hasta ahora se hace notar que nos falta pulir algunas cosas, agregar los estilos faltantes para que el sitio se vea como en la imagen demo. Agregar un estilo más, que no se observa en la imagen demo, el cual es cambiar el color del fondo de los inputs y textareas cuando se da foco en ellos. *Repasar el uso de `position` puliendo los estilos para las clases guitarras y guitarra.*

	***Estilos faltantes:***
	* __Gradients__: [Entendiendo el uso de gradientes.](https://developer.mozilla.org/es/docs/CSS/Using_CSS_gradients) [Generador de gradientes](http://www.cssmatic.com/es/gradient-generator "de cssmatic") [de mozilla](http://www.colorzilla.com/gradient-editor/ "de mozilla").
	* __Perfiles__: [Entender el uso de perfiles en css _(La propiedad `outline`)_](http://librosweb.es/libro/css_avanzado/capitulo_4/propiedad_outline.html)

	---

Hasta aquí hemos finalizado todos los pasos de la capacitación.

---

### Los objetivos de esta capacitación

La capacitación número 1 tiene como propósito hacer que las personas que lo realizaron puedan:

- Entender HTML, como empezar con la maquetación, identificar que etiquetas usar dependiendo del *bosquejo* o *imagen demo*.
- Entender CSS, el uso de los selectores y poder identificar que propiedades usar para dar estilos según nuestro *bosquejo*.
- Por ultimo, y no menos importante, es que a partir de ahora en adelante al momento que se tenga que modificar algún archivo HTML y/o CSS lo hagamos sin temor a romper lo ya hecho.

---

Si al finalizar la capacitación no te sientes identificado con ninguno de los objetivos, entonces puede que falte pulir y/o agregar mayor detalle y explicaciones a la capacitación para la mejor comprensión, o puede que simplemente necesites hacerlo de nuevo pero esta vez leyendo todos los enlaces externos que están referenciados en cada paso.

---

Si te das cuenta que falta o se puede mejorar algún detalle, no dudes en enviar tu [Pull Request](http://aprendegit.com/que-es-un-pull-request/ "Qué es un pull request?") al proyecto para poder analizar la necesidad de agregar tus cambios.

__________________________________________________________________________________
__________________________________________________________________________________